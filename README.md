# a24-conventions

## Введение 

Это руководство по созданию читаемого, расширяемого, адаптированного к многоразовому использованию кода на PHP. 
Каждое правило должно строго соблюдаться.
 

## Содержание

1. [Стандарты](#markdown-header-1)
2. [БД](#markdown-header-2) 
3. [Классы](#markdown-header-3)
4. [Модели](#markdown-header-4-eloquent)
5. [Методы](#markdown-header-5)
6. [Переменные](#markdown-header-6)
7. [Комментарии](#markdown-header-7)
8. [Общие принципы](#markdown-header-8)
9. [Grapqh](#markdown-header-9-graphql) _(в разработке)_
10. [Event dispatcher](#markdown-header-10-event-dispatcher) _(в разработке)_
11. [Тесты. phpunit](#markdown-header-11)
12. [Типичные имена переменных для нашего проекта](#markdown-header-12)
13. [Кеш](#markdown-header-13)
14. [Исключения](#markdown-header-14)
15. [Архитектура](#markdown-header-15) _(в разработке)_
16. [IDE](#markdown-header-16)
17. [Модуляр](#markdown-header-17)
18. [Ветки](#markdown-header-18)

## 1. [Стандарты](#markdown-header-a24-conventions)
### PSR
Код должен соответствовать всем правилам, перечисленным в [PSR-2](https://www.php-fig.org/psr/psr-2/), если иное не указано в данном документе.

### RFC
В руководстве используется стандарт [RFC 2119](http://www.ietf.org/rfc/rfc2119.txt) ([русская версия](http://rfc.com.ru/rfc2119.htm))

  Понятие                     | Пояснения                                                                                                   | Антоним                                                                                                             
 :---------------------------:| :-----------------------------------------------------------------------------------------------------------| :--------------:  
  ОБЯЗАТЕЛЬНО (MUST)          | Без условий и исключений                                                                                    | НЕДОПУСТИМО                                                                                          
  РЕКОМЕНДУЕТСЯ (RECOMMENDED) | Можно отступать от правила, если понимаете правило и понимаете почему оно не работает в этом частном случае | НЕ РЕКОМЕНДУЕТСЯ  
  ПО ЖЕЛАНИЮ (OPTIONAL)       | Один из способов реализации                                                                                 |                                                                                                                    


## 2. [БД](#markdown-header-a24-conventions)

### 2.1 Имена таблиц
РЕКОМЕНДУЕТСЯ именовать таблицы во множественном числе, а все модели и контроллеры - в единственном. 

Неправильно:   
```php
<?php

class Users extends Model 
{
    protected $tableName = 'user';
}

class Users extends Model 
{
    protected $tableName = 'users';
}
```
    
Правильно:
```php
<?php

class User extends Model 
{
    protected $tableName = 'users';
}

class Meta extends Model 
{
    protected $tableName = 'users_meta';
}
```
 
  
### 2.2 Именование полей в таблицах

РЕКОМЕНДУЕТСЯ

* Названия полей, состоящие из нескольких слов, писать через подчеркивания. 
* Все имена должны быть осмыслены. 
* Нельзя называть поля одинаковыми именами с разными цифрами в окончании (abc2, abc3 и т.д.)
* Нельзя называть поля транслитом
* Нельзя называть поле номером таски из jira
* Сокращения должны быть понятными
* Для дат использовать названия в стиле created_at, updated_at, deleted_at

Неправильно:
```sql
    CREATE TABLE users (
        ipk int,
        ps int,
        isActiveCustomer string,
        pochta STRING,
        web9153 bool,
        json JSONB
    );
```

Правильно:
```sql
    CREATE TABLE users (
        id int,
        password int,
        is_active INT(2),
        email_confirmed STRING,
        enable_chat bool,
        options JSONB
    );
```

### 2.3 Id
РЕКОМЕНДУЕТСЯ во всех таблицах создавать поле id и primaryKey.
Исключение могут составлять таблицы, хранящие связь "многие-ко-многим".

```sql
    CREATE TABLE order (
        id int,
        title STRING,
        ...
    );
    CREATE TABLE offer (
        id int,
        title STRING,
        ...
    );
    CREATE TABLE order_offers (
        order_id int,
        offer_id int,
    );
```


### 2.4 Поля id на другие сущности
ОБЯЗАТЕЛЬНО поля содержащие id на другие таблицы именовать как название сущности в единственном числе с окончанием id.

Неправильно:
```sql
    CREATE TABLE user_meta (
        ...
        user int
        ...
    );
```

Правильно:
```sql
    CREATE TABLE user_meta (
        ...
        user_id int
        ...
    );
``` 


### 2.5 Именование булевых переменных
РЕКОМЕНДУЕТСЯ все поля boolean типа писать с префиксом `is` или `has`, `can`.
 
Неправильно:
```sql
    CREATE TABLE user_meta (
        ...
        admin boolean
        chat  boolean       
        ...
    );
```

Правильно:
```sql
    CREATE TABLE user_meta (
        ...
        is_admin boolean
        has_chat  boolean
        ...
    );
```


### 2.6 У полей должны быть комментарии
Поля помимо стандартных (id, name) ОБЯЗАТЕЛЬНО должны иметь комментарии в таблице.
> Цель: Понимание семантики данных таблицы без изучения кода и снижение ошибок хранения разных данных в одном поле.


## 3. [Классы](#markdown-header-a24-conventions)
### 3.1 Модификатор видимости констант

ОБЯЗАТЕЛЬНО указывать модификатор видимости констант.

Неправильно:
```php
<?php

class Order extends Model
{
    const FLAG_WAS_AUTO_ANSWERED = 1 << 0;
}
```

Правильно:
```php
<?php

class Order extends Model
{
    public const FLAG_WAS_AUTO_ANSWERED = 1 << 0;
}
```

### 3.2 Порядок определения констант, свойств и методов
При описание класса ОБЯЗАТЕЛЬНО сначала указываем трейты, потом константы, затем свойства, затем методы класса. Сортировка внутри сущности 
осуществляется от public к protected и private.

Абстрактные методы должны предшествовать остальным публичным методам.  

Неправильно:
```php
<?php

class Order
{
    public function getComments()
    {
        /** ... */
    }
    
    private $performer;
    public const WARRANTY_LENGTH = 86000;
    use SoftDeleteTrait;
}
```

Правильно:
```php
<?php

class Order
{
    use SoftDeleteTrait;
    
    public const WARRANTY_LENGTH = 86000;
    
    private $performer;
    
    public function getComments()
    {
        /**  */
    }
}
```

### 3.3 Сеттеры

РЕКОМЕНДУЕТСЯ возвращать текущий объект в сеттерах.

```php
<?php

class User
{
    private $name;
    private $surname;
    private $flag;
    
    // неправильно
    public function setFlag(int $flag): boolean 
    {
        $this->flag = $flag;
        return true;
    }
    
    // правильно
    public function setName(string $name): User
    {
        $this->name = $name;
        return $this;
    }
    
    // правильно
    public function setSurname(string $surname): User
    {
        $this->surname = $surname;
        return $this;
    }
}
```

Это позволит составлять цепочки сеттеров:
```php
<?php

$user = (new User())
    ->setName('UserName')
    ->setSurname('Surname');
```

### 3.4 Магические числа
В коде ОБЯЗАТЕЛЬНО должны отсутствовать магические числа. Все числа ОБЯЗАТЕЛЬНО выносятся в константы с "говорящими" названиями.

Неправильно:
```php
<?php

public function isCustomer() : bool
{
    return ($this->group_id === 2);
}
```    

Правильно:
```php
<?php

class User extends Model implements UserInterface
{
    public const GROUP_CUSTOMER = 2;   

    public function isCustomer() : bool
    {
        return ($this->group_id === self::GROUP_CUSTOMER);
    }
}   
```

### 3.5 Именование классов
РЕКОМЕНДУЕТСЯ не дублировать неймспейс в названии класса. 
РЕКОМЕНДУЕТСЯ использовать постфиксы Event, Listener, Command, Handler, Buffer. 
ОБЯЗАТЕЛЬНО не называть общими словами, которые не передают значение класса (Trash, Service, Main).

Неправильно:
```
    \App\Articles\Handbook\Service\HandbookArticle::getHandbookImageUrlForArticle
```

Правильно:
```
    \App\Handbook\Service\Article::getImageUrl
```

### 3.6 По умолчанию объявлять методы РЕКОМЕНДУЕТСЯ как private 

### 3.7 Статические вызовы 
Статические вызовы РЕКОМЕНДУЕТСЯ делать только у самого класса. 
У экземпляра можно обращаться только к его свойствам и методам.

Неправильно:
```php
<?php

$user = User::find($id);

$role = $user::getRoleNameByGroupId($otherId);

```

Правильно:
```php
<?php

$user = User::find($id);

$role = User::getRoleNameByGroupId($otherId);

```

### 3.8 РЕКОМЕНДУЕТСЯ соответствие SOLID принципу

## 4. [Модели eloquent](#markdown-header-a24-conventions) 

### 4.1 Базовый класс
Все модели ОБЯЗАТЕЛЬНО наследуются от Model/Models.

Неправильно:
```php
<?php

use Illuminate\Database\Eloquent\Model;

class Referer extends Model 
{

}
```

Правильно:
```php
<?php

use Models\Model;

class Referer extends Model 
{

}
```

### 4.2 Relations
РЕКОМЕНДУЕТСЯ называть relations: 

* связь N к N - во множественном числе,
* связь N к 1 - во множественном числе,
* связь 1 к N - в единственном числе.

### 4.3 Возвращаемые значения при поиске моделей
Функции, которые используют `query builder` и отвечают за поиск одной  или нескольких моделей (не sphinx поиск), 
ОБЯЗАТЕЛЬНО должны возвращать объект модели или объект коллекции. Если модель не найдена, РЕКОМЕНДУЕТСЯ возвращать null.

Неправильно:
```php
<?php
class BlackList extends Model 
{
    public static function getPerformers(int $userID) : ?array
    {    
        $result = self::query()
            ->where('customer_id', $userID)
            ->get();

        if (!empty($result)) {
            return $result->keyBy('author_id')->toArray();
        }
    
        return null;
    }
}
```
Правильно:
```php
<?php
class BlackList extends Model 
{
    public static function getPerformers(int $userID) : Collection
    {    
        $performers = self::query()
            ->where('customer_id', $userID)
            ->get();

        if (!$performers->isEmpty()) {
            $result = $performers->keyBy('author_id');
            
            return $result;
        }
    
        return new Collection();
    }
}
```

### 4.4 Что должно быть в моделе
 * Все что связано с описанием сущности (аттрибуты, $fillable, $guarded, преобразование типов $casts и т.д.)
 * Accessors & Mutators, сетеры и геттеры
 * Описание сценариев валидации, ошибок валидации, кастомных сценариев валидации
 * Все отношения модели (Relation) к другим моделям
 * Все Scope, которые относятся к сущности
 * Методы, связанные с поиском модели или коллекции моделей, должны быть только в моделе. И находиться так, чтобы сократить время поиска нужной функции другими программистами до минимального.
 * Проверки, которые относятся к этой модели и зависят от аттрибутов модели (например isCustomer, hasComments)

Что не должно быть в моделях

* Внутри модели не использовать другие модели напрямую (только если через relation)
* Внутри модели не использовать сторонние сервисы
* Сложную логику сохранения, поиска, проверки и т.д. выносить в сервисы (например создание комментария)
* Не переопределять стандартную функцию rules (см. ValidationTrait)
* Не использовать старые модели (не Eloquent) и DAO

### 4.5 Запросы через query
Запросы (Query Builder) к моделям должны быть ОБЯЗАТЕЛЬНО через статический метод query()

Неправильно:
```php
<?php

	$performers = self::where('customer_id', $userID)->get();
```

Правильно:
```php
<?php
	$performers = self::query()
		->where('customer_id', $userID)
		->get();
```

### 4.6 Правильно использование scope
Scope - именованое условие,  позволяют записывать повторяющиеся конструкции where (допускается также join, sort, select) 
в запросах моделей в виде короткого метода, с говорящим называнием.  
Они определяются в модели в виде метода, начинающегося с scope.

Scope должны быть небольшими, РЕКОМЕНДУЕТСЯ использовать один-два where, связанных по смыслу. 
 
Если набор из нескольких scope повторяется в нескольких местах, его РЕКОМЕНДУЕТСЯ вынести в сервис или в один scope с говорящем названием, 
но внутри НЕДОПУСТИМО использование условных операторов if, case или логики управления другими scope.

Сервисы которые по данным составляют запрос из разных scope модели, должны ОБЯЗАТЕЛЬНО иметь только одну, эту, обязанность. 

ОБЯЗАТЕЛЬНО давать понятные и говорящие имена scope, scopeOfIsActive намного понятнее назвать scopeActive()

Как не надо делать:

* Внутри scope не надо делать limit, offset
* Внутри scope вызывать другие scope, прямым вызовом $this->scopeX 
* Делать scope не понятными, передавать массив данных, и внутри функции составлять запрос из нужных условий
* Scope делать private или protected
* Возвращать builder раньше условия

Неправильно:
```php
<?php

     // использование offset и limit		
	 public function scopeOfListInAdminPanel(Builder $builder, ?int $offset, ?int $perPage, array $filters=[])
        {
            $builder = $this->scopeOfFiltersForListInAdminPanel($builder, $filters);
            $builder->orderBy('id', 'desc');
            $builder->offset($offset ?? 0);
            if ($perPage!==null) {
                $builder->limit($perPage);
            }
    
            return $builder;
        }
        
        // использование функции напрямую
        public function scopeOfCountListInAdminPanel(Builder $builder, array $filters=[])
		{
			$builder = $this->scopeOfFiltersForListInAdminPanel($builder, $filters);
			$builder->selectRaw('COUNT(id) as total');
			return $builder;
		}
		
		// приватный scope и управление другими scope 
		private function scopeOfFiltersForListInAdminPanel(Builder $builder, array $filters)
		{
			if (isset($filters['key_referer']) && $filters['key_referer'] != '') {
				$builder = $this->scopeOfKeyLike($builder, $filters['key_referer']);
			}
			if (isset($filters['group_referer']) && (int)$filters['group_referer'] != 0) {
				$builder = $this->scopeOfGroupId($builder, $filters['group_referer']);
			}
			if (isset($filters['title']) && $filters['title'] != '') {
				$builder = $this->scopeOfTitleLike($builder, $filters['title']);
			}
			if (isset($filters['label_referer']) && (int)$filters['label_referer'] != 0) {
				$builder = $this->scopeOfLabelId($builder, $filters['label_referer']);
			}
			return $builder;
		}
		
		/**
		* преждевременный return $builder
		*
 		* при использовании $builder->active(null)->get() 
 		* запрос будет SELECT * FROM table, что на больших объемах данных НЕДОПУСТИМО   
		*/
		public function scopeActive(Builder $builder, $value)
		{
			if ($value===null) {
				return $builder;
			}
			$builder->where('isactive', true);
			return $builder;
		}
```

Правильно:
```php
<?php
	public function scopeByStage(Builder $query, int $stage): Builder
	{
		$query->where('stage', $stage);

		return $query;
	}
	
	public function scopeHasNotComplainInHandle(Builder $query): Builder
	{
		$query->doesntHave('handledComplain');

		return $query;
	}
	
	public function scopeSortByLastActivity(Builder $query): Builder
	{
		$query->orderByRaw('dates.last_activity DESC NULLS LAST');

		return $query;
	}
	
	public function scopeSearch(Builder $query, string $searchQuery): Builder
	{
		$escapedSearchQuery = pg_escape_string($searchQuery);

		/** @var Order|Builder $query */
		$query->where(
			function (Builder $subQuery) use ($escapedSearchQuery) {
				return $subQuery->where('hiddenInfo', 'ilike', "%{$escapedSearchQuery}%")
					->orWhere('info.title', 'ilike', "%{$escapedSearchQuery}%")
					->orWhere('info.description', 'ilike', "%{$escapedSearchQuery}%")
					->orWhere('performer.nick_name', 'ilike', "%{$escapedSearchQuery}%");
			}
		);

		return $query;
	}
```


## 5. [Методы](#markdown-header-a24-conventions)
### 5.1 Количество аргументов в методе
РЕКОМЕНДУЕТСЯ использовать не более 3 аргументов в методе. 
Если требуется больше, РЕКОМЕНДУЕТСЯ передать данные объектами, либо путем изменения состояния объекта через сеттеры.

> Цель: 
> - Улучшение читаемости кода. 
> - Уменьшение возможности ошибки при передаче параметров

Неправильно:
```php
<?php

class Orders_Service_Main
{
    public function getOrderListByAuthorId($userId = 0, $page = 1, $perPage = -1, $stage, $eventSort = false)
    {
        $userId = (int)$userId;
        if (!$userId) return false;
        $page = (int)$page;
        $perPage = (int)$perPage;
        $orderService = Orders_Service_Order::getInstance();
        $orderList = $orderService->getOrderListByAuthorId($userId, $page, $perPage, $stage, $eventSort);
        return $orderList;
    }
}

$orderService = new Orders_Service_Main();
$orders = $orderService->getOrderListByAuthorId($userId, 3, 30, STAGE_FINISH, true);
```

Правильно:

```php
<?php

class Orders_Service_Main
{
    private $page = 1;
    private $perPage = -1;
    private $eventSort = false;
    
    public function getOrderListByAuthorId(int $userId, int $stage)
    {
        $orderService = Orders_Service_Order::getInstance();
        $orderList = $orderService->getOrderListByAuthorId([
            "user_id"  => $userId,
            "page"     => $this->page,
            "limit"    => $this->perPage,
            "stage"    => $stage, 
            "eventSort"    => $this-eventSort
        ]);
        return $orderList;
    }
    
    public function setPage(int $page)
    {
        $this->page = $page;
        return $this;
    }
    
    public function setLimit(int $limit)
    {
        $this->perPage = $limit;
        return $this;
    }
    
    public function enableEventSort()
    {
        $this->eventSort = true;
        return $this;
    }
}

$orderService = new Orders_Service_Main();
$orderService
    ->setPage(3)
    ->enableEventSort()
    ->setLimit(30)
    ->getOrderListByAuthorId($userId, STAGE_FINISH);
```

### 5.2 Изменение входных параметров функции
НЕДОПУСТИМО изменять входные параметры функции.

Неправильно:
```php
<?php

public static function getTaxInfo($amount, $paymentMethod)
{
    // .. 
    $ourTax = \Base_Service_Payment_Payment::getOurTaxPercent();
    $amount = round($amount / $this->prepareTax($ourTax), self::ROUND);
    // ..
    $amount = round($amount * $parametersPaymentMethod['taxHidden'], self::ROUND);
    // ..
    $data['amount'] = $amount;
    return $data;
}
```

Правильно:
```php
<?php

public static function getTaxInfo($amount, $paymentMethod)
{
    // .. 
    $ourTax = \Base_Service_Payment_Payment::getOurTaxPercent();
    $amountWithTax = round($amount / $this->prepareTax($ourTax), self::ROUND);
    // ..
    $amountWithoutTax = round($amount * $parametersPaymentMethod['taxHidden'], self::ROUND);
    // ..
    $data['amount'] = $amount;
    $data['amountWithTax'] = $amountWithTax;
    $data['amountWithoutTax'] = $amountWithoutTax;
    return $data;
}
```
### 5.3 Возвращаемые типы данных
НЕ РЕКОМЕНДУЕТСЯ:

 * вариативность возвращаемых типов данных, за исключением null
 * зависимости от типа аргументов
 * возвращать mixed

Неправильно:
```php
<?php

/**
 * @return bool|Landing
 */ 
public function makeLanding(int $groupId)
{
    if (!$groupId) {
        return false;
    }
    
    $className = $this->getLandingsClassName($groupId);
    
    if ($className) {
        return new $className();
    }

    return false;
}
```

Правильно:
```php
<?php

/**
 * @return null|Landing
 */ 
public function makeLanding(int $groupId): ?Landing
{
    if (!$groupId) {
        return null;
    }
    
    $className = $this->getLandingsClassName($groupId);
    
    if ($className) {
        return new $className();
    }

    return null;
}
```

### 5.4 Отрицательные логические названия методов

НЕ РЕКОМЕНДУЕТСЯ давать отрицательные логические названия методов.

Неправильно:
```php
<?php

if ($user->isNotPartner){
    //...
}

if ($order->hasNotPerformer){
    //..
}
```
Правильно:
```php
<?php

if (!$user->isPartner){
    //..
}

if (!$order->hasPerfomer){
    //..
}
```

### 5.5 Названия методов

НЕ РЕКОМЕНДУЕТСЯ использовать слова из имени класса в имене метода.

Неправильно:
```php
<?php

    \App\Handbook\Service\Article::getHandbookImageUrlForArticle
```

Правильно:
```php
<?php

    \App\Handbook\Service\Article::getImageUrl
```

Допустимо:
```php
<?php

    \App\User\Ban::ban();
    \App\User\Ban::reject();
```

### 5.6 РЕКОМЕНДУЕТСЯ количество if в функции не больше 3

> Что делать? Объединять одинаковые if, выносить в методы if, относящиеся к одной абстракции, к одному функционалу.
> Не стоит выносить в отдельные методы несвязанные куски кода только ради того, чтобы уменьшить кол-во if.

Неправильно:
```php
<?php

public function redirectByTypeAndCategory(?int $typeId, ?int $categoryId)
{
    if (is_null($typeId)) {
        show_404();
    }
    if (is_null($categoryId) && is_null($typeId)) {
        show_404();
    }
    $typeUrl = $this->generateDictUrl($typeId, 'work_types');
    if (is_null($typeUrl)) {
        show_404();
    }
    if (is_null($categoryId)) {
        $this->redirectToLenta([$typeUrl]);
    }
    $subjectsUrl = $this->generateDictUrl($categoryId, 'work_categories');
    if (is_null($subjectsUrl)) {
        $this->redirectToLenta([$typeUrl]);
    }
    $this->redirectToLenta([$typeId, $categoryId]);
}
```

В примере выше видно, что есть проверки на входные параметры для показа 404 и есть проверки для редиректа.
Объединим проверки на валидность входных параметров в функцию.

Правильно:
```php
<?php

public function redirectByTypeAndCategory(?int $typeId, ?int $categoryId)
{
    if (! $this->isValidVariables($typeId, $categoryId)) {
        show_404();
    }
    
    $typeUrl = $this->generateDictUrl($typeId, 'work_types');
    if (is_null($categoryId)) {
       $this->redirectToLenta([$typeUrl]);
    }
     
    $subjectsUrl = $this->generateDictUrl($categoryId, 'work_categories');
    if (is_null($subjectsUrl)) {
        $this->redirectToLenta([$typeUrl]);
    }
            
    $this->redirectToLenta([$typeId, $categoryId]);
}
```

### 5.7 Порядок передачи параметров в метод 
Параметры в методах РЕКОМЕНДУЕТСЯ использовать в следующем порядке: 
`обязательные` → `часто используемые` → `редко используемые`

Неправильно:
```php
<?php
public function all(int $limit, ?int $stage = null, UserModel $performer, int $lastCommentId) : Collection
```

Правильно:
```php
<?php
public function all(UserModel $performer, int $lastCommentId, int $limit, ?int $stage = null) : Collection

// Еще лучше
public function all(UserModel $performer, int $lastCommentId, ?int $stage = null) : Collection
{
    // ..
    $limit = $this->getLimit();
}
```

### 5.8 Boolean в параметрах 
НЕ РЕКОМЕНДУЕТСЯ передавать boolean параметры в функцию.

> Цель:

* Читаемость. Сеттеры или функции меняющие состояние/флаги выглядят понятнее и читабельнее
* Сохранение инкапсуляции. Не нужно залезать в функцию, чтобы узнать на что влияют boolean переменные
* Полиморфизм. Создание функций с разделением логики в каждой подталкивает к использованию нескольких классов
* Стиль ООП. Вынесение переменных в поля объекта и создание функций, работающих с этими полями, приближает код к ООП
* Соответствие SOLID принципу. Функция должна выполнять только одну обязанность. Но она может быть управляющей и внутри решать, какой другой функции 
 передать управление

> Возможные решения проблемы:

* Создать в объекте аргумент и написать функцию, которая будет управлять этим аргументом
* Функцию, логика которой разделяется в зависимости от boolean переменной, разбить на две независимые функции.
Использовать их по отдельности или через управляющую, но без дублирования условия по классу
* Выделить разделяющуюся логику в отдельные классы, убрать дублирубщие условия


Неправильно:
```php
<?php

$abtest = new Abtest();
$abtest->setUser($user, true, true, false);


public function setUser(?User $user = null, $add = true, $rewrite = false, $group = false) : ABTest
{
    // ..
}
```

Правильно:
```php
<?php

$abtest = new Abtest();
$abtest->setUser($user)
    ->findGroupIfNotExist()
    ->disableRewriteGroup();

$abtest->setUser($user)
    ->setGroup(1);

```


Неправильно:
```php
<?php

$markup = (new Prerender($this->router->uri->uri_string))->take(true);

class Prerender
{
    use TimerTrait;

    /** @var Request $request */
    private $request;

    public function __construct(string $location)
    {
        $this->setLocation($location);
    }
    
    public function take($useStream)
    {
        if ($useStream) {
            $post = json_encode($this->data);
            $this->send($useStream, $post);
        }
        
        $post = http_build_query($this->data);
        $this->send($useStream, $post);        
    }
    
    public function send($useStream, $data) 
    {
        if ($useStream) {
            // .. stream_socket_client()
        } else {
            // .. $curl = curl_init();
        }
    }
}
```

Правильно:
```php
<?php

// 1.
$markup = (new Prerender($this->router->uri->uri_string))->setStream(true)->take();

// 2. 
$markup = (new Prerender($this->router->uri->uri_string))->setSender(new Stream())->take();

// 3.
class Prerender
{
    use TimerTrait;

    private $request;
    private $enableStream;

    public function __construct(string $location)
    {
        $this->setLocation($location);
    }
    
    public function setSteam($enableStream) 
    {
        $this->enableStream = $enableStream;
        
        return $this;
    }
    
    public function take()
    {
        return ($this->getSender())->send($this->getData());        
    }
    
    private function getSender()
    {
        if ($this->enableStream) {
            return new StreamSender();
        }
        
        return new CurlSender();
    }
}
```

### 5.9 НЕ РЕКОМЕНДУЕТСЯ смешивать в одной функции разные уровни абстракции

### 5.10 Одна причина для изменения  
РЕКОМЕНДУЕТСЯ чтобы функция имела только одну причину для изменения.
НЕ РЕКОМЕНДУЕТСЯ создавать функции которые выполняют больше одного действия.

## 6. [Переменные](#markdown-header-a24-conventions)

### 6.1 Тип данных в название переменной 
НЕДОПУСТИМО в именовании переменной указывать тип данных array, string, int, float и т.д.
Исключение составляет указание модели или сервиса.

Неправильно:
```php
<?php

$ids_array = [1, 2, 3];
$intOrderId = 123;
```

Правильно:
```php
<?php

// @var array $ids 
$ids = [1, 2, 3];

// @var int $orderId
$orderId = 123;

// @var User $userModel
$userModel = User::find(1);

// @var UserProfile $profileService
$profileService = new Profile($user);
```

### 6.2 Именование переменных CamelCase

ОБЯЗАТЕЛЬНО именование переменных CamelCase

Неправильно:
```php
<?php

$order_id = $order_model->id;

$USERNAME = $user->name;

$isusercustomer = $user->isCustomer();

$HasBan = $user->hasBan();
```

Правильно:
```php
<?php

$orderId = $order->getId();

$orderId = $dates->order_id;

$userName = $user->name;

$isCustomer = $user->isCustomer();

$hasBan = $user->hasBan();
```

### 6.3 Именование переменной содержащей Id

ОБЯЗАТЕЛЬНО

Неправильно:
```php
<?php

// если один id
$orderID;
$idOrder;
$idorder;
$IDorder;
$id_order;
$order_id;
$oid;
$OID;

// если массив ids
$order_ids;
$ordersId;
$idsOrder;
$orderListId;
$oids;
$orders;
```

Правильно:
```php
<?php

// если один id
$orderId;

// если массив ids
$orderIds;
```

### 6.4 Место инициализации переменной 

РЕКОМЕНДУЕТСЯ инициализировать переменные максимально близко к месту первого использования.

Неправильно:
```php
<?php

public function order(int $fakeId)
{
    // ...
    // line 423
    $createUrl = Promo_Service_Promo::getInstance()->getUrlBySubjectAndType($data['order']['category_id'], $data['order']['type_id']); 
    // ...
    // line 530        
    if(empty($createUrl)) {
        $createUrl = Promo_Service_Promo::getInstance()->getUrlBySubjectAndType($data['order']['type_id']);
    }
    // ...
}
```

Правильно:
```php
<?php

public function order(int $fakeId)
{
    // ...
    $createUrl = Promo_Service_Promo::getInstance()->getUrlBySubjectAndType($data['order']['category_id'], $data['order']['type_id']);        
    if(empty($createUrl)) {
        $createUrl = Promo_Service_Promo::getInstance()->getUrlBySubjectAndType($data['order']['type_id']);
    }  
    // ...
} 
```

### 6.5 Переменная номером задачи
НЕДОПУСТИМО называть переменную номером таска, с любым префиксом и постфиксом.

Неправильно:
```php
<?php
$web5313 = $this->getOrderForWeb5313();

$isWeb5313 = $this->USER->isAdmin();
```

Правильно:
```php
<?php

$activeUsers = $this->getActiveUsers();

$isAdmin =  $this->USER->isAdmin();

```

### 6.6 Общие правила именования переменных 
* Название переменных ОБЯЗАТЕЛЬНО должно соответствовать содержанию
* НЕДОПУСТИМО писать короткие названия, например $c
* Переменные РЕКОМЕНДУЕТСЯ называть на корректном английском
* НЕ РЕКОМЕНДУЕТСЯ в названиях использовать числа, например $is80percent, лучше $isPercentPassed
@todo нужны примеры 


### 6.7 НЕДОПУСТИМО использовать сокращения сущностей

Неправильно:
```php
<?php
    $u = User::find($id);
    $dt = new BaseDate();
    $prfm = User::find($pid);
```

Правильно:
```php
<?php
    $user = User::find($id);
    $deadline = new BaseDate();
    $performer = User::find($performerId);
```

## 7. [Комментарии](#markdown-header-a24-conventions)

### 7.1 Закомментированный код
Закомментированный код ОБЯЗАТЕЛЬНО удалять. Почти всегда можно восстановить из GIT'а.

### 7.2 Символы для комментирования
В php-файлах для комментариев ОБЯЗАТЕЛЬНО использовать только ```//``` и ```/* */```.

Неправильно:
```php
<?php

### web-xxx
return str_replace(self::PLACEHOLDER, $this->injection, $renderedHTML);
```

Правильно:
```php
<?php

// если рефок было слишком мало, то остальные баннеры будут идти без рефок
return str_replace(self::PLACEHOLDER, $this->injection, $renderedHTML);
```

### 7.3 Осмысленные комментарии
НЕДОПУСТИМО дублировать код комментариями. 
Хороший комментарий отвечает на вопрос "Почему?", а не "Как?". Стоит комментировать только не очевидные, запутанные решения, родившиеся в результате проб, ошибок, обхода подводных камней и исполнения безумных желаний менеджера.

Неправильно:
```php
<?php

// ищем юзеров по id
$users = User::find($userIds);

// хэшируем строку
$hash = md5($string);
```

Правильно:
```php
<?php

/**
 * Дано:
 *  Пришел email
 *  У нас есть пользователь с таким email
 *  У нас нет записи с таким social_id
 *
 * Результат:
 *  Создаем запись UserSocial, авторизуем юзера
 */
if (isset($user) && !isset($userSocial)) {
    // здесь какой-то код
}

/**
 * Дано:
 *  Пришел email
 *  У нас есть пользователь с таким email
 *  У нас есть запись с таким social_id
 *
 * Результат:
 *  Если user_id в записе users_social совпадает с id найденного пользователя - авторизуем
 *  Если не совпадает - ошибка, соц. аккаунт привязан к другому пользователю
 */
if (isset($user) && isset($userSocial)) {
    // и здесь тоже
}

```

### 7.4 НЕДОПУСТИМО обрамлять комментариями код

Неправильно:
```php
<?php

// ----отправляем в стату реги для WEB-7920
$homeStat = new \App\Stat\Service\HomePage($userObj->id);
$homeStat->incReg(\App\Stat\Service\HomePage::ENTRY_NORMAL);
// ---- //


// ----------------- отправляем в стату реги для WEB-7920 ----------
$homeStat = new \App\Stat\Service\HomePage($userObj->id);
$homeStat->incReg(\App\Stat\Service\HomePage::ENTRY_NORMAL);
// ------------------------------------------------------------- 


// *** МОЙ КОД ****
$homeStat = new \App\Stat\Service\HomePage($userObj->id);
$homeStat->incReg(\App\Stat\Service\HomePage::ENTRY_NORMAL);
// #### мой код закончился ####
```

Правильно:
```php
<?php

// отправляем в стату реги для WEB-7920
$homeStat = new \App\Stat\Service\HomePage($userObj->id);
$homeStat->incReg(\App\Stat\Service\HomePage::ENTRY_NORMAL);
```

### 7.5 Исключения в phpdoc
Если в методе используется исключение, это ОБЯЗАТЕЛЬНО должно быть указано в phpdoc метода.
 
Неправильно:
```php
<?php

/**
 * Шаблон для письма, в зависимости от группы
 * @return string
 */
private function getTemplate(): string
{
    if ($this->isCustomer()) {
        return $this->templateCustomer;
    }

    if ($this->isAuthor()) {
        return $this->templateAuthor;
    }

    throw new \Exception('group_id not CUSTOMERS or AUTHORS');
}

```

Правильно:
```php
<?php

/**
 * Шаблон для письма, в зависимости от группы
 * @return string
 * @throws \Exception
 */
private function getTemplate(): string
{
    if ($this->isCustomer()) {
        return $this->templateCustomer;
    }

    if ($this->isAuthor()) {
        return $this->templateAuthor;
    }

    throw new \Exception('group_id not CUSTOMERS or AUTHORS');
}
```

## 8. [Общие принципы](#markdown-header-a24-conventions)

### 8.1 Для работы с датами ОБЯЗАТЕЛЬНО используем Base_Date
> Цели:
> - беспроблемная работа с времеными зонами
> - единообразие работы с датам, временем, таймзонами

Неправильно:
```php
<?php

class Base_Service_Log{
    
    private static function prepareMessage($message, $date)
    {
        return date('d.m.Y H:i:s', $date).' '.$message."\r\n";
    }
}
```
Правильно:
```php
<?php

class Base_Service_Log{
    
    private static function prepareMessage($message, $date)
    {
        return (new Base_Date($date))->format('d.m.Y H:i:s.u');
    }
}
```
### 8.2 Использование избыточных условий
НЕДОПУСТИМО использовать else, если есть возможность продолжить код без вложенности.

Неправильно:
```php
<?php

public function __construct($time = 'now', $timezone = self::UTC_LOCALE)
{
    $this->setDefaults();

    if (is_numeric($time)) {
        $dateTime = sprintf('@%d', $time);
        $utcForce = true;
    } else {
        $dateTime = $time;
        $utcForce = false;
    }

    $currentTimeZone = $this->getTimezoneByInternal($timezone);

    parent::__construct($dateTime, $currentTimeZone);

    if ($utcForce) {
        $this->setTimezone($currentTimeZone);
    }
}
```

Правильно:
```php
<?php

public function __construct($time = 'now', $timezone = self::UTC_LOCALE)
{
    $this->setDefaults();
    
    $dateTime = $time;
    $utcForce = false;
    if (is_numeric($time)) {
        $dateTime = sprintf('@%d', $time);
        $utcForce = true;
    }

    $currentTimeZone = $this->getTimezoneByInternal($timezone);

    parent::__construct($dateTime, $currentTimeZone);

    if ($utcForce) {
        $this->setTimezone($currentTimeZone);
    }
}
```
### 8.3 Неиспользуемый код
Если код не должен исполняться - НЕ РЕКОМЕНДУЕТСЯ его оставлять.

Неправильно:
```php
<?php

if (false) {
    legacyMethodCall();
}
// ...
$legacyCondition = true;
if ($legacyCondition) {
    finalizeData($data);
}
```

Правильно:
```php
<?php

finalizeData($data);
```

### 8.4 Размер управляющих констуркций
Размер управляющей конструкции РЕКОМЕНДУЕТСЯ не более 20 строк.

### 8.5 Размер вложености управляющих конструкций
Вложеность управляющих конструкций РЕКОМЕНДУЕТСЯ не больше двух.

### 8.6 Для сравнения с null ОБЯЗАТЕЛЬНО использовать ===
Потому что к булевым значениям приводится всё что угодно, в переменных может быть false,
подразумевающий что это не null, и всё это сложно отловить потом, потому что не всегда 
проявляется. Функция is_null() очень медленная по сравнению с '==='.

Неправильно:
```php
<?php

if ($order == null) {
}

if (is_null($order)) {
}
```

Правильно:
```php
<?php

if ($order === null) {
}
```

### 8.7 НЕДОПУСТИМО использовать Yoda conditions
Код предназначен в первую очередь для чтения, а читать Йоду сложнее из-за нарушения шаблона. Достоинство Йоды (нельзя написать одно равно вместо нескольких) реализуется один раз (при написании) и нивелируется одновременным написанием юнит тестов, а недостаток (затруднение чтения) проявляться будет всегда.

Неправильно:
```php
<?php

if (null === $orderId) {
   //...
}

if (false === $hasComment) {
    //...
}
```

Правильно:
```php
<?php

if ($orderId === null) {
   //...
}

if ($hasComment === false) {
   //...
}
```

### 8.8 НЕДОПУСТИМО использовать результат операции присваивания, в том числе в условных операторах. 
Такие штуки сложно читать, обычно строки с такими вещами длинные и запутанные.
Кроме того, такое поведение может быть не явным (это магическое наследие первых версий 
PHP и рано или поздно оно изменится).
Лучше явно указывать результат или проверку, и потом if'ать именно его.

Неправильно:
```php
<?php
// 1 
if ( false === ($data['user'] = $this->users_model->getUserInfo($userID)) )
     show_error('Такого пользователя не существует');

// 2     
if ($user = User::find($id)) {
    $avatar = $user->getAvatar();
}     
```

Правильно:
```php
<?php

$user = User::find($userId);
if($user === null){
    show_error('Такого пользователя не существует');
}
```

### 8.9 НЕДОПУСТИМО нескольким переменным присваивать одно и то же значение. 
Если несколько переменных одинаковые, то непонятно откуда брать информацию и где 
её изменять. Один программист подумает, что очевидно вот с этой переменной работать,
а другой будет работать с другой. В результате могут быть неожидаемые изменения или
не быть ожидаемых.

Неправильно:
```php
<?php

// получение пользователя и проверка существования
if ( false === ($data['user'] = $this->users_model->getUserInfo($userID)) )
    show_error('Такого пользователя не существует');

// инфо об админе
$adminInfo = $this->users_model->getUserInfo($this->session->userdata('user_id'));

// получение информации о пользователе
$user = $this->users_model->getUserInfo($userID);
if ( $user['group_id'] == CUSTOMERS )
    $user['group'] = 'users';
if ( $user['group_id'] == AUTHORS )
    $user['group'] = 'users';

```

Правильно:
```php
<?php

$user = User::find($userId);
if($user === null){
    show_error('Такого пользователя не существует');
}
$admin = $this->userModel;
```

### 8.10 В условном операторе ОБЯЗАТЕЛЬНО проверять boolean значение 
Потому что всё приводится к булевому типу, и многие ошибки могут быть пропущены.
Если же мы примиеняем специфические операции для различных типов, для приведения 
к булевому значению, непредусмотренное значение сразу же свалится с ошибкой.

Неправильно:
```php
<?php

$this->userIds = $this->filterSuitableUsers($users, false, $force);
if ($this->userIds) {
    //...
}
return $this->userIds;

```

Правильно:
```php
<?php

$this->userIds = $this->filterSuitableUsers($users, false, $force);
if (count($this->userIds) == 0) {
    return [];
}

//...

return $this->userIds;
```


### 8.11 Условие ОБЯЗАТЕЛЬНО должно быть понятным
Обилие условий резко снижает скорость чтения и усложняет понимание кода.

Неправильно:
```php
<?php

if (!isset($newData['stage']) || $newData['stage'] != STAGE_HIDDEN) {
    //...
}
```

Правильно:
```php
<?php

$isGoingToHidden = ($newData['stage'] ?? '' == STAGE_HIDDEN)
if(!$isGoingToHidden){
    //...
}
```

### 8.12 Boolean переменные РЕКОМЕНДУЕТСЯ определять в скобках
При беглом чтении операцию присваивания легко спутать со сравнением и неправильно понять код, из-за чего придется вернуться к определению и перечитать его. Определение в скобках наглядно и не даёт такого эффекта.

Неправильно:
```php
<?php

$isDark = $color == $black;
```

Правильно:
```php
<?php

$isLight = ($color == $white);
```

### 8.13 PHPDOC и type hinting
Входные параметры, возвращаемое значение, исключения, и краткое описание функции должно быть ОБЯЗАТЕЛЬНО описано в phpdoc. ОБЯЗАТЕЛЬНО должен быть указан тип входных данных и возвращаемого значения.

Неправильно:
```php
<?php

/**
 * @return mixed
 */
private function takeMarkup($preloadState)
{
    $this->getRequest()->setPreloadState($preloadState);

    $result = $this
        ->getMarkupSender()
        ->setRequest($this->getRequest())
        ->take();

    return $result;
}
```

Правильно:
```php
<?php

/**
 * Управляющая функция для получения верстки
 *
 * @param array $preloadState состояние пользователя на момент рендера
 * @throws ErrorConnectException 
 * @return null|string
 */
private function takeMarkup(array $preloadState): ?string
{
    $this->getRequest()->setPreloadState($preloadState);

    $result = $this
        ->getMarkupSender()
        ->setRequest($this->getRequest())
        ->take();

    return $result;
}
```

### 8.14 ОБЯЗАТЕЛЬНО Instanceof вместо устаревшей is_a

Неправильно:
```php
<?php

public function getTokenStr($user, int $ttl = self::DEFAULT_LIFETIME) : string
{
    if (is_a($user, '\Base_Model_User')) {

        return 'user=' . $user->getId() . '&token=' . $this->getToken($user->getId(), $user->get('key_mail'));
    }
    // ...   
}
```

Правильно:
```php
<?php

public function getTokenStr($user, int $ttl = self::DEFAULT_LIFETIME) : string
{
    if ($user instanceof \Base_Model_User) {

        return 'user=' . $user->getId() . '&token=' . $this->getToken($user->getId(), $user->get('key_mail'));
    }
    // ..   
}
```

### 8.15 Возвращаемая переменная 
Для переменной результата функции РЕКОМЕНДУЕТСЯ использовать имя $result или имя сущности из названия функции. 
Для обозначения результата работы со сторонними сервисами или api использовать имя $response.
В любом месте в методе должно быть понятно, где вы оперируете результатом, а где локальными переменными.

Неправильно:
```php
<?php

// 1
public function getCountAuthorsMakeBid(array $params) : int
{
    $result = CrmAuthors::query()
            ->distinct()
            ->leftJoin('offers', 'offers.user_id', '=','crm_authors.user_id');
            if (isset($params['dateFrom'])) {
                $result->where('offers.created_at', '>',$params['dateFrom']);
            }
            if (isset($params['dateTo'])) {
                $result->where('offers.created_at', '<=', $params['dateTo']);
            }
            
    $count = $result->count('crm_authors.user_id');
    return $count;
}

// 2
public function getCountAuthorsMakeBid(array $params) : int
{
    $request = CrmAuthors::query();
            // ..
            
    $response = $request->count('crm_authors.user_id');
    return $response;
}

```

Правильно:
```php
<?php

// 1
public function getCountAuthorsMakeBid(array $params) : int
{
    $query = CrmAuthors::query()
            ->distinct()
            ->leftJoin('offers', 'offers.user_id', '=','crm_authors.user_id');
            if (isset($params['dateFrom'])) {
                $query->where('offers.created_at', '>',$params['dateFrom']);
            }
            if (isset($params['dateTo'])) {
                $query->where('offers.created_at', '<=', $params['dateTo']);
            }
    $count = $query->count('crm_authors.user_id');
    
    return $count;
}


// 2
public function getCountAuthorsMakeBid(array $params) : int
{
    // .. 
    $result = $query->count('crm_authors.user_id');
    
    return $result;
}

// 3
public function getCountAuthorsMakeBid(array $params) : int
{
    // .. 
    return $query->count('crm_authors.user_id');
}
```

### 8.16 Постфикс Trait
Все трейты должны ОБЯЗАТЕЛЬНО иметь потсфикс Trait

Неправильно:
```php
<?php

class UserLoadBuffer
{
    use Buffer;
 }
```

Правильно:
```php
<?php
  
class UserLoadBuffer
{
    use BufferTrait;
 }
```

### 8.17 Постфикс для интерфейсов
При именовании интерфейса ОБЯЗАТЕЛЬНО использовать одно из правил:

* Добавить в окончание able
* Если название получается нечитабельное, использовать постфикс Interface

### 8.18 Switch
РЕКОМЕНДУЕТСЯ `switch` использовать только для полиморфизма и возвращения разных объектов. 
НЕДОПУСТИМО использовать `switch(true)`

### 8.19 ОБЯЗАТЕЛЬНО Использовать битовые маски как 1 << 45;

## 9. [Graphql](#markdown-header-a24-conventions)
 
### 9.1
@todo graphql логика в мутациях и запросах
### 9.2
@todo graphql решение проблемы N+1 использовать класс Buffer

### 9.3 При указании типа для поля id (любой сущности) ОБЯЗАТЕЛЬНО использовать Type::id 

Неправильно:
```php
<?php

public function args(): array
{
    return [
        'orderId' => Type::nonNull(Type::int()),
    ];
}

Order::getName() => [
    'type'        => SingletonPool::getInstance(Order::class),
    'description' => _('Информация о заказе'),
    'args'        => [
        'id' => self::nonNull(Type::string()),
    ],
    ...
```

Правильно:
```php
<?php

public function args(): array
{
    return [
        'orderId' => Type::nonNull(Type::id()),
    ];
}

Order::getName() => [
    'type'        => SingletonPool::getInstance(Order::class),
    'description' => _('Информация о заказе'),
    'args'        => [
        'id' => self::nonNull(Type::id()),
    ],
    ...
```

### 9.4 
@todo graphql выносить общий функционал кастомера и перформера в base 

## 10. [Event Dispatcher](#markdown-header-a24-conventions)

### 10.1 Терминология
* *Event*. Каждое событие должно быть структурой данных, то есть объектом, являющимся 
контейнером для данных и имеющее методы для извлечения этих данныхз. Ничего другого 
там быть не должно.
* *Listener*. Слушатели  это управляющие объекты, отправляющие в необходимые сервисы 
необходимые данные из события.
* *Event Dispatcher*. Это сервис, связывающий событие с его слушателями.
* *Предметная область бизнес логики*. Всё, что касается бизнес логики и не 
касается реализации.
* *Объект предметной области*. Сущность, легко выделяемая в предметной области, обладающая свойствами
, способная манипулировать другими объектами бизнес логики или быть манипулируемой. 

### 10.2 События в системе

 У нас есть 3 системы событий.
 * **Старые события** (например *application/libraries/App/Orders/Service/Events.php* 
 или *application/libraries/App/User/Service/Events.php*). Минус этой системы в 
 слишком большой связности кода и трудности поддержания. Предполагается постепенный
 отказ от этой системы.
 * События **Eloquent**-моделей. Эти события связаны с работой с моделями и не должны 
 касаться предметной области бизнес логики.
 * **Event Dispatcher**. События в нём касаются только объектов предметной области. Этот 
 раздел посвящен именно этим событиям. 

### 10.3 Концепция

> Цель: 
> - Уменьшить связанность кода
> - Сделать прозрачной и предсказуемой обработку событий

 Для некоторых объектов предметной области легко выделить состояние, которое менятся во 
 времени в зависимости от ситуации. Это состояние оказывает влияние на предметную область бизнес 
 логики. 
 
 Например заказ может быть в одном из нескольких состояний - черновик, в аукционе и т.п.
 Или пользователь может быть забанен или не забанен. Очевидно, что это значимые для предметной 
 области состояния. 

 В то же время есть менее значимые состояния, кеоторые оказывают значительно меньшее влияние 
 на предметную область. Например состояние заголовка заказа. Технически заголовок заказа и 
 состояние заказа вещи малоотличающиеся, но для нашей предметной области влияне этих свойств
 сильно отличается. Изменение существующего заголовка не влияет практически не на что. Так же 
 мало на что влиют многие другие свойства заказа, такие как количество страниц или процент 
 уникальности. 
 
 Сравнивая эти два свойства заказа становится видно, что в нашей предметной области есть объект
 статуса заказа, но нет объекта заголовка заказа. Со временм это может измениться, но сейчас
 дела обстоят именно так. 
 
 **Event Dispatcher** должен обслуживать именно объекты предметной области, поэтому для 
 заголовка заказа НЕДОПУСТИМО создавать события, а для статуса заказа - ОБЯЗАТЕЛЬНО.
 
 Какие именно события мы должны создавать?
 
 Определившись с объектом, следующим шагом будет выделение его состояний. И мы можем создать
 события на:
 * на каждое возможное изменение состояния
 * на каждое начальное состояние (в каком состоянии объект был до изменения)
 * на каждое конечное состояние (в каком состоянии объект оказался после изменения)  
 
 Выбор принципа создания событий зависит от доменной области, но НЕ РЕКОМЕНГДУЕТСЯ их смешивать.
 
 После создания событий, на нужные из них вешаются слушатели, чьей единственной задачей является
 при срабатывании события запустить некие сторонние сервисы, которые должны что-то сделать.

### 10.4 Разница между Eloquent и EventDispatcher

 Единственная разница в области ответсвенности: Eloquent отвечает за реализацию, а EventDispatcher
 за бизнес логику. 

 Вещи, которые должны быть выполнены на уровне PHP для поддержания консистентности 
 системы ОБЯЗАТЕЛЬНО должны быть в Eloquent событиях. Например при изменении модели инвалидировать кеш или 
 при создании новой модели создать связанные модели.
 
 Относящееся к предметной области бизнес логики ОБЯЗАТЕЛЬНО должно быть в eventDispatcher.
 Например отправить пользователям письмо при новой ставке или собрать статистику при регистрации
 нового пользователя.
 
### 10.5 Именование событий и слушателей 
* Классы событий должны ОБЯЗАТЕЛЬНО иметь потсфикс Event;
* Классы слушателей должны ОБЯЗАТЕЛЬНО иметь постфикс Listener;
* НЕДОПУСТИМО чтобы имя класса события и слушателя было образовано от одного слова/сущности, это говорит
о нарушении концепции евент диспетчера (см 10.2, 10.3)
* НЕДОПУСТИМО называть слушателя общим именем например EventListener, EventHandler, т.к. становится неочевидно,
 что происходит в слушателе

Неправильно:
```php
<?php

return [
    Ban::class => [
        \Closure::fromCallable([Service\Ban::class, 'handle']),
        \Closure::fromCallable([BanHandler::class, 'web123'])
    ],
    Event\Logout::class => [
        \Closure::fromCallable([Listener\EventProxy::class, 'logout'])
    ],
];

// ---

return [
    Reject::class         => [
        Closure::fromCallable([EventListener::class, 'reject'])
    ],
    Assessment::class    => [
        Closure::fromCallable([EventListener::class, 'assessment'])
    ],
    Approve::class       => [
        Closure::fromCallable([EventListener::class, 'approve'])
    ],
    Work::class          => [
        Closure::fromCallable([EventListener::class, 'work'])
    ],
    Success::class       => [
        Closure::fromCallable([EventListener::class, 'success'])
    ],
];
```

Правильно:
```php
<?php

return [
    BanEvent::class => [
        \Closure::fromCallable([NotifyListener::class, 'ban']),
        \Closure::fromCallable([LoggerListener::class, 'ban'])
    ],
    LogoutEvent::class => [
        \Closure::fromCallable([EventProxyListener::class, 'logout'])
    ],
];

// ---

return [
    RejectEvent::class         => [
        Closure::fromCallable([EventProxyListener::class, 'reject'])
    ],
    AssessmentEventr::class    => [
        Closure::fromCallable([EventProxyListener::class, 'assessment'])
    ],
    ApproveEvent::class       => [
        Closure::fromCallable([EventProxyListener::class, 'approve'])
    ],
    WorkEvent::class          => [
        Closure::fromCallable([EventProxyListener::class, 'work'])
    ],
    DoneEvent::class       => [
        Closure::fromCallable([EventProxyListener::class, 'success'])
    ],
];
```
### 10.6 Правильное использование событий
* НЕДОПУСТИМО чтобы в классе события были функции выполняющую бизнес логику, только методы работы с данными
* Событие ОБЯЗАТЕЛЬНО должен оповещать только об одном событии/действии бизнес логики. 
Например событие Ban не может иметь флаг, по которому он начинает делать событие разбана.
 
### 10.7 Правильное использование слушателей
* НЕДОПУСТИМО в качестве слушателя использовать сервис, модель, контроллер.
Cервисы, модели и т.д. не должны завязываться на объект Event
* НЕДОПУСТИМО реализовавыть логику в слушателе
* Все функции слушателя ОБЯЗАТЕЛЬНО должны относится к одной абстракции и функциональности. 
Слушатель не может и записывать в лог и отдавать данные в евент прокси, и инкрементить стату, это три разных слушателя. 

### 10.8 Расположение слушателей и евентов
Слушатели и События должны ОБЯЗАТЕЛЬНО располагаться в соответствующих директориях Events и Listeners, в директории
сущности, событие которой происходит, на одном уровне с Service
 
Правильно:
```
App
    Orders
        Service
        Events 
        Listeners
```

### 10.8 Опасности в Eloquent моделях

При создании слушателя ОБЯЗАТЕЛЬНО надо убедиться, чтобы ни при каких условиях не происходило
 зацикливание этого события. Например при сохранении объекта можно сделать слушателя, который
 изменяет модель и сохраняет его. При этом вызовется событие, которое активирует этого же 
 слушателя и произойдёт зацикливание. Или изменение одной модели может вызвать изменение другой,
  которая вызывает изменение первой. 

## 11. [Юнит тесты](#markdown-header-a24-conventions)

### 11.1 РЕКОМЕНДУЕТСЯ писать юнит тесты
[Почему нужны юнит тесты](https://habr.com/company/sberbank/blog/354096/)

### 11.2 ОБЯЗАТЕЛЬНО Пишите осмысленные сообщения

В assert есть параметр (обычно третий), в котором можно передать сообщение, которое 
будет показано при ошибке в этом сравненни. Надо его писать так, чтобы при срабатывании
было понятно что пошло не так. Обычно в момент написания теста программист глубоко в
теме, поэтому всё кажется очевидным, а при срабатывании теста программист давно уже забыл
о чем речь, и ему приходится заново всё читать.

Не правильно
```php
<?php

$result = $service->accrRequestStatusesByProjectId(Project::PROJECT_UK);
$expected = [6, 7, 8];
$this->assertEquals($expected, $result);
//...
$result = $service->accrRequestStatusesByProjectId(Project::PROJECT_UK);
$expected = [6, 7, 8];
$this->assertEquals($expected, $result, 'expected not right');
```
Правильно
```php
<?php

$result = $service->accrRequestStatusesByProjectId(Project::PROJECT_UK);
$expected = [6, 7, 8];
$this->assertEquals($expected, $result, 'Неправильно определяются статусы проекта');
```
### 11.3 ОБЯЗАТЕЛЬНО в dataProvider надо указывать название группы данных

В дата провайдерах можно не указывать название группы данных, но делать этого не надо. 
Потому что тогда при описании ошибки будет показан весь набор данных, который очень плохо читатется с экрана.
И придется проходится по всем группам вручную, чтобы найти какая группа упала.

Неправильно:
```php
<?php

return [
     // email isValid
     ['test@test.ru', true ],
     ['invalidEmail', false],
     // ...
];
```
Правильно:
```php
<?php

return [
    'valid e-mail' => [
        'test@test.ru', 
        true 
    ],
    'email without @' => [
        'invalidEmail', 
        false
    ],  
];
```
### 11.4 Группировка тестов

ПО ЖЕЛАНИЮ у юнит теста в аннотации можно указывать группу, чтобы при разработке запускать
только свои тесты. Это уменьшает время выполнения тестов и позволяет сконцентрироваться только 
на своей работе.

Пример аннотации:
```php
 /**
     * @param $post
     * @param $attributeName
     * @param $expected
     *
     * @dataProvider dataProviderForPublicAttributes
     * @group commandBusd rfatirt
     */
    public function testPublicAttributes($post, $attributeName, $expected, $get = [])
    {
        $target = new FormDataContainer($post, $get);
        $this->assertSame($expected, $target->$attributeName, "Фиаско с {$attributeName}");
    }
```

Пример запуска таких тестов:
```
phpunit -c phpunit.xml --group=commandBus
```


## 12. [Типичные имена переменных](#markdown-header-a24-conventions)
```
$user - пользователя
$customer - заказчик
$performer - автор
$order - заказ
$complain - жалоба
$offer - предложение автора
$bid - ставка
$stage - статус заказа 
```

@todo: если посмотреть в словарь, то вообще 'complain' - это глагол, т.е. жаловаться, подать жалобу, а 'complaint' - существительное, т.е. жалоба. У нас в проекте почти везде используется complain, хотя в паре мест я видел и complaint. Стоит обсудить - смиримся мы с этой ошибкой и будем использовать complain (каждый раз вытирая кровавые слезы) или заморочимся и будем переходить на complaint. 

## 13. [Кеш](#markdown-header-a24-conventions)

### 13.1 При кешировании ОБЯЗАТЕЛЬНО использовать классы
 для PSR-6 `\Base\Cache` или для PSR-16 `\Base\TagAwareCache`
 
### 13.2 Для выставления TTL ОБЯЗАТЕЛЬНО использовать класс Ttl
 Класс \Base\Service\Cache\Ttl реализует интерфейс HasTimeTtl, константы с временем должны быть там

### 13.3 Для ключа кеша ОБЯЗАТЕЛЬНО использовать константы из класса \Base\Service\Cache\Key
Для ключа кеша ОБЯЗАТЕЛЬНО использовать константы из класса \Base\Service\Cache\Key

### 13.4 ОБЯЗАТЕЛЬНО придерживаться правила именования ключа кеша
Для ключа кеша ОБЯЗАТЕЛЬНО использовать константы из класса \Base\Service\Cache\Key

Правило формирование ключа { prefix } . { название }
Зумза обрезает только два перввых слова по точке
     
 1. Это результат запроса?              -> префикс db         + название запроса или таблицы или смысл запроса
 2. Это модель eloquent?                -> префикс model      + название модели (в этом случае в кеше должна быть модель)
 3. Это старая модель?                  -> префикс old_model  + название модели
 4. Это результат вычисления функции?   -> префикс result     + названние функции или смысл действия
 5. Не называть ключи в стиле WEB34534, Trash, и т.д.
 6. В ключе НЕ РЕКОМЕНДУЕТСЯ использовать `{`, `}`, `(`, `)`, `/`, `\\`, `@`, `:`
        
Правильно:
```php
 <?php
  
 const DB_FORUM_ACTION_LIST = 'db.forum_action_list';
 const DB_PROMO_BY_URL = 'db.promo_by_url';

 const MODEL_HANDBOOK_LAST_ARTICLES = 'model.handbook_articles.last';
 const MODEL_USER = 'model.user';

 const OLD_REF_INFO_BY_KEY = 'old_model.ref_info_by_key';
 const OLD_USERS_BY_EMAILS = 'old_model.users_by_emails';

 const RESULT_GET_AUTHORS_COUNT = 'result.result_authors_count';
 const RESULT_CHECK_PINCODE_USER = 'result.checkPincodeUser';
 
 const COLLECTION_PSP_CONST_TAX_PAYMENT = 'collection.psp_const_tax_payment';
 const COLLECTION_PSP_METHOD_INFO       = 'collection.psp_method_info';
```

### 13.5 РЕКОМЕНДУЕТСЯ указывать дефолтное значение null, для функции get()

Неправильно:
```php
<?php

$result = $cache->get($key, false);
if ($result) {
    ... 
}

$result = $cache->get($key, 0);
if ($result !== 0) {
    ... 
}
```

Правильно:
```php
<?php

$result = $cache->get($key, null);
if ($result !=== null) {
    ... 
}

```

## 14. [Исключения](#markdown-header-a24-conventions)

### 14.1 Исключения ОБЯЗАТЕЛЬНО бросаются и ловятся на разных уровнях

Исключение бросаетя, когда код не может на своём уровне решить, что в такой ситуации надо делать. Соответсвенно ловится исключение на том уровне абстракции, где обработка этого исключения входит в круг обязанностей кода. Поэтому на одном и том же уровне исключения ловить не слудет - если уровень абстракции способен обработать исключение, то он не должен его бросать, а должен сам обработать ситуацию, без исключений. Если же код не имеет возможности обработать исключение, то он и не должен его обрабатывать.

> Цель
> - Использование одного принципа работы с исключениями. 
> - Более глубокое разделение уровней абстракции.

Неправильно
```php
<?php

class WrongException
{
    public function proccess($input){
         try{
             $this->checkInput($input);
             return 'ok';
         }catch (Exception $exception){
             return 'notOk';
         }
    }
    
    /**
    * @param $input
    * @throws Exception
     */
    private function checkInput($input){
        if(empty($input)){
            throw new \Exception('Hello');
        }
    }
}
```

Правильно

```php
<?php

class WrongException
{
    public function proccess($input){
         try{
             $isCorrect = $this->checkInput($input);
             if(!$isCorrect){
                return 'notOk';
             }
             processDb($input);
             return 'ok';
         }catch (DBException $exception){
             return 'notOk';
         }
    }
    
    /**
    * @param $input
    * @throws Exception
     */
    private function checkInput($input):bool{
        if(empty($input)){
            return false;
        }
        return true;
    }
}
```

## 15. [Архитектура](#markdown-header-a24-conventions) _(к обсуждению)_ 

![picture](architecture.jpg)



## 16. [IDE](#markdown-header-a24-conventions)

###16.1 Code style для phpStorm
ПО ЖЕЛАНИЮ можно использовать настроенный кодстайл для шторма.

Лежит в Standards/PhpStorm/a24codeStyle.xml

**Как установить:**
В шторме нажать **Ctrl+Alt+S** (+ win на убунте) (что бы открыть настройки). Дальше 
**Editor->Code style->PHP** 

Там нажать кнопку Manage и выбрать Import.

Там выбрать Intellij IDEA codstyle XML. Ну а дальше открыть нужный файл.

#### Как использовать кодстайл
Выделить кусок кода (если не выделить, весь файл отформатируется).
И нажать Ctrl+Alt+L (+ win на убунте) или меню Code->Reformat Code


###16.2 Inspections для phpStorm
ПО ЖЕЛАНИЮ можно использовать настроенный набор отображаемых предупреждений для шторма.

Лежит в Standards/PhpStorm/a24inspections.xml

**Как установить:**
В шторме нажать **Ctrl+Shift+S** (чтобы открыть настройки). Дальше 
**Editor->Inspections->PHP** 

Там нажать кнопку Manage и выбрать Import. Там открыть нужный файл.

#### Как использовать инспекции
Будет подсвечиваться код, где что то не так. Чтобы исправить, поставить курсор на выделенный участок 
и нажать Alt+Enter, чтобы шторм сам поправил.
Или можно правой кнопкой мыши по файлу или папке с файлами и выбрать **Inspect Code...**

###16.3 Шаблоны пустых файлов для phpStorm
ПО ЖЕЛАНИЮ можно использовать настроенные заготовки пустых файлов для шторма.

Лежит в Standards/PhpStorm/SettingsTemplates.jar

**Как установить:**
В шторме выбрать меню **File->Import Settings...**. Там выбрать путь до файла, открыть его и применить настройки.

## 17. [Модуляр](#markdown-header-a24-conventions)
(modular - модульный)

Основная идея паттерна - разделение управляющей логики, бизнес логики и состояния (входящего и исходящего). Описанные ниже компоненты - лишь один из вариантов реализации паттерна. 

### 17.1 Context
Контекст хранит в себе входящее состояние. Контекст задается при инициализации модуляра и не меняется в процессе его работы. Базовый класс - ModularContext.

Все данные складываются в protected массив $data. Для работы с данными реализован ряд public методов в базовом классе. Как правило, есть конечный список переменных, которые могут передаваться конкретному модуляру в качестве переменных контекста. В этом случае РЕКОМЕНДУЕТСЯ создать свой Контекст, наследующийся от базового класса, и в public константах описать ключи всех возможных переменных контекста.

### 17.2 Container
Контейнер хранит в себе исходящее состояние и временные данные. Если внутри модуляра вам нужно передать какие-то данные между классами, это нужно делать через контейнер. Так же в контейнер нужно положить результат работы модуляра. Базовый класс - ModularContainer.

В базовом классе реализован ряд переменных и методов, однако они там скорее для примера (или в случае, если вам достаточно описанных там переменных, тогда не обязательно создавать свой класс Контейнера). Можно создавать любое необходимое количество public переменных и методов-оберток для данных.

### 17.3 Environment
Окружение - обертка над контекстом и контейнером, хранит в себе их экземпляры и умеет передавать их классам, реализующим интерфейс ModularSystemModuleInterface (описан ниже).

### 17.4 Processor
В текущей реализации процессор отвечает за большую часть управляющей логики. Базовый класс - ModularProcessor. В процессоре допускается реализация только той части кода, что отвечает за управление процессом работы модуляра. Хоть процессор и хранит Environment (а в нем Container и Context), в 95% случаев ему к ним точно не нужно обращаться (особенно к контейнеру).

Минимальный набор для запуска процессора - Контекст (входящее состояние), Контейнер (исходящее состояние) и Стратегия (бизнес-логика). 

### 17.5 Strategy
Стратегия - основное хранилище бизнес-логики. В контексте архитектуры модуляра, стратегия - основной строительный материал. Каждая стратегия описывает какое-то одно (в масштабах модуляра) действие. Стратегия может вызывать сервисы и модели, но основные данные, влиющие на результат работы модуляра, нужно выносить в контекст, собирая их в момент инициализации. Например: модель жалобы для системы перерасчетов это основные данные, т.к. от наличия\отсутствия модели зависит, сможем ли мы обработать жалобу и как мы ее обработаем. В то же время, наша комиссия автора при перерасчете влияет лишь на то, какую сумму мы возвращаем автору, не влияя на процесс обработки жалобы глобально, а значит эти данные можно запросить в самой стратегии.

В простейшем случае (или при прототипировании) можно ограничиться одними стратегиями, вручную добавляя их в процессор при инициализации.

В стратегии нельзя добавлять\убирать схемы и\или стратегии в процессор.

### 17.6 Schema
Схема - обертка над стратегиями. Стратегия, в идеале, описывает какое-то одно действие (в масштабах модуляра), схемы же позволяют объединить несколько действий в одну последовательность. Например: есть стратегии "валидация данных", "проверка прав доступа", "создание жалобы", а все вместе это оборачивается в схему "пользователь создает жалобу".

В схеме можно использовать данные контекста для ветвления логики посредством добавления в процесессор различного набора стратегий. В схеме нельзя использовать контейнер (на момент вызова схем он вообще должен быть пустым, т.к. схемы конфигурируются до стратегий, а в контейнер данные кладут только стратегии). В схеме можно добавлять в процессор другие схемы, но это крайне не рекомендуется. Если возникла такая необходимость, скорее всего, нужно как-то расширить функционал процессора или добавить дополнительные управляющие классы.

### 17.7 Интерфейсы
Все классы, кроме контекста и контейнера, должны реализовывать интерфейс ModularSystemModuleInterface, описывающий set\get методы для Контекста и Контейнера.

Все схемы, помимо ModularSystemModuleInterface, должны так же реализовывать интерфейс ModularSchemaInterface, описывающий метод configure.

Все стратегии, как и схемы, реализуют свой интерфейс ModularStrategyInterface, описывающий методы prepare, run и shouldProcess.

### 17.8 Варианты расширения
Тут позже напишу варианты расширения паттерна.




## 18. [Ветки](#markdown-header-a24-conventions)

###18.1 Именование веток
Именование WEB-NNNN. ОБЯЗАТЕЛЬНО большими буквами, с дефисами.
Если нужна еще одна ветка той же таски - WEB-NNNN-korotkoe-poyasnenie-4erez-defisi-malenkimi-bykvami
Каждый commit message начинаем с WEB-NNNN
Неправильно:
```php
<?php

WEB10976
web10976
WEB_10976
web_10976
web-10976
10976
WEB/10976
```

Правильно:
```php
<?php

WEB-10976
WEB-10976-sb
WEB-10976-close-abtest

```






```


---- добавить ----

- именование веток
- в сервсисе не должно быть прямых удаление и апдейтов через queryBuilder 
- дерево моделей, куда класть модель
- ServiceABTestWeb9658 названия классов в аб тесте
- $ABTestWeb9658 = \App\ABTest\Web9658\FactoryABTestWeb9658::makeTest($user);
- $ABTestWeb9658->amountOfOrders(1);
- Не использовать так сервисы App\Orders\Service\Rework::find($data['reworkId']);
- Проверка модели === null, использовать exists
- getOrdersViewDataForWeb8259 в моделе так нельзя называь
- НЕДОПУСТИМО использовать сокращения переменных $oDt $valDt  
- название веток и описание к коммитам
- InputType
- неймспейсы не групировать
- анонимные классы
- find для методов получения данных в моделе
- в папке с моделями должны лежать только модели

@todo:
- Форматирование править.
- не работают ссылки
- примеры, там где нет или где можно лучше
- упрощение названий getAdditRefAdditInfoForAuthors
- $cache = new \Base\Cache();
- Event  с постфиксом и Listener с постфиксом
- аб тесты

