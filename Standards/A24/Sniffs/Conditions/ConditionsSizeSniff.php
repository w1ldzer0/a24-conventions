<?php
/**
 * This sniff prohibits the use of Perl style hash comments.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   PHP_CodeSniffer
 * @author    Your Name <you@domain.net>
 * @license   https://github.com/squizlabs/PHP_CodeSniffer/blob/master/licence.txt BSD Licence
 * @link      http://pear.php.net/package/PHP_CodeSniffer
 */

namespace PHP_CodeSniffer\Standards\A24\Sniffs\Conditions;

use PHP_CodeSniffer\Sniffs\Sniff;
use PHP_CodeSniffer\Files\File;

class ConditionsSizeSniff implements Sniff
{
    const MAX_CONDITION_SIZE = 20;

    private $name = 'conditionMaxSize';
    /**
     * Returns the token types that this sniff is interested in.
     *
     * @return array(int)
     */
    public function register()
    {
        return array(T_IF);

    }

    /**
     * Processes this sniff, when one of its tokens is encountered.
     *
     * @param \PHP_CodeSniffer\Files\File $phpcsFile The current file being checked.
     * @param int                         $stackPtr  The position of the current token in the
     *                                               stack passed in $tokens.
     *
     * @return void
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();

        $opener = $tokens[$stackPtr];
        if (!isset($opener['scope_closer'])){
            return;
        }
        $closer = $tokens[$opener['scope_closer']];
        if ($closer['line'] - $opener['line'] > self::MAX_CONDITION_SIZE){
            $error = 'Слишком большое условие';
            $phpcsFile->addWarningOnLine($error, $opener['line'], $this->name, []);
        }
    }


}

