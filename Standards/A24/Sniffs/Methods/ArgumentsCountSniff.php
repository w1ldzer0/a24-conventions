<?php

namespace PHP_CodeSniffer\Standards\A24\Sniffs\Conditions;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;

class ArgumentsCountSniff implements Sniff
{
    private const MAX_COUNT = 3;

    private $name = 'methodCountArguments';
    /**
     * Returns the token types that this sniff is interested in.
     *
     * @return array(int)
     */
    public function register(): array
    {
        return array(T_FUNCTION);
    }

    /**
     * Processes this sniff, when one of its tokens is encountered.
     *
     * @param \PHP_CodeSniffer\Files\File $phpcsFile The current file being checked.
     * @param int                         $stackPtr  The position of the current token in the
     *                                               stack passed in $tokens.
     *
     * @return void
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();
        $parameters = $phpcsFile->getMethodParameters($stackPtr);
        $methodInFile = $tokens[$stackPtr];
        if (count($parameters) > self::MAX_COUNT){
            $error = 'Большое количество параметров в методе';
            $phpcsFile->addWarningOnLine($error, $methodInFile['line'], $this->name, []);
        }
    }

}