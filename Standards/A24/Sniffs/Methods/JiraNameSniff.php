<?php

namespace PHP_CodeSniffer\Standards\A24\Sniffs\Methods;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;

class JiraNameSniff implements Sniff
{
    private $name = 'methodName';
    /**
     * Returns the token types that this sniff is interested in.
     *
     * @return array(int)
     */
    public function register(): array
    {
        return [T_FUNCTION];
    }

    /**
     * Processes this sniff, when one of its tokens is encountered.
     *
     * @param \PHP_CodeSniffer\Files\File $phpcsFile The current file being checked.
     * @param int                         $stackPtr  The position of the current token in the
     *                                               stack passed in $tokens.
     *
     * @return void
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $methodName = $phpcsFile->getDeclarationName($stackPtr);

        $tokens = $phpcsFile->getTokens();
        $methodInFile = $tokens[$stackPtr];

        if (preg_match('/^.*(web?)[\d\- ]{2,10}$/i', $methodName, $matches)){
            $error = 'Метод не должен использовать номер таски в названии ' . $methodName;
            $phpcsFile->addWarningOnLine($error, $methodInFile['line'], $this->name, []);
        }
    }

}