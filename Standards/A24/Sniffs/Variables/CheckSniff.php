<?php

namespace PHP_CodeSniffer\Standards\A24\Sniffs\Variables;

use PHP_CodeSniffer\Sniffs\Sniff;
use PHP_CodeSniffer\Files\File;

class CheckSniff implements Sniff
{
    public function register()
    {
        return array(T_LIST);
    }

    public function process(File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();

        $variable = $tokens[$stackPtr];
        //var_dump($variable);

    }
}
