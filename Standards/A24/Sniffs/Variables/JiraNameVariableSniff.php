<?php

namespace PHP_CodeSniffer\Standards\A24\Sniffs\Variables;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;

class JiraNameVariableSniff implements Sniff
{
    private $name = 'variableName';
    /**
     * Returns the token types that this sniff is interested in.
     *
     * @return array(int)
     */
    public function register(): array
    {
        //return [T_LIST];
        return [T_VARIABLE];
    }

    /**
     * Processes this sniff, when one of its tokens is encountered.
     *
     * @param \PHP_CodeSniffer\Files\File $phpcsFile The current file being checked.
     * @param int                         $stackPtr  The position of the current token in the
     *                                               stack passed in $tokens.
     *
     * @return void
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();
        $variable = $tokens[$stackPtr];

        if (preg_match('/^.*(web?)[\d\- ]{2,10}$/i', $variable['content'], $matches)){
            $error = 'Переменная не должна называться как номер таски ' . $variable['content'];
            $phpcsFile->addWarningOnLine($error, $variable['line'], $this->name, []);
        }
    }

}