<?php

namespace PHP_CodeSniffer\Standards\A24\Sniffs\Variables;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;

class JiraNameClassSniff implements Sniff
{
    private $name = 'className';
    /**
     * Returns the token types that this sniff is interested in.
     *
     * @return array(int)
     */
    public function register(): array
    {
        return [
            T_CLASS,
            T_INTERFACE,
            T_TRAIT,
        ];
    }

    /**
     * Processes this sniff, when one of its tokens is encountered.
     *
     * @param \PHP_CodeSniffer\Files\File $phpcsFile The current file being checked.
     * @param int                         $stackPtr  The position of the current token in the
     *                                               stack passed in $tokens.
     *
     * @return void
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $className = $phpcsFile->getDeclarationName($stackPtr);
        $tokens = $phpcsFile->getTokens();
        $data = $tokens[$stackPtr];
        if (preg_match('/^.*(web?)[\d\- ]{2,10}$/i', $className, $matches)){
            $error = 'Название класса не должно содержать номер таски ' . $className;
            $phpcsFile->addWarningOnLine($error, $data['line'], $this->name, []);
        }
    }

}