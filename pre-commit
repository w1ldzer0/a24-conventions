#!/bin/bash

REDCOLOR='\033[0;31m'
GREENCOLOR='\033[0;32m'
NOCOLOR='\033[0m'

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PHPCS_BIN=$DIR/vendor/bin/phpcs
PHPCBF_BIN=$DIR/vendor/bin/phpcbf
STANDARD=$DIR/Standards/A24
IFS='
'

if [ -z "$PHP_BIN" ]
then
        PHP_BIN=php
fi

errors=""
if ! which "$PHP_BIN" >/dev/null 2>&1
then
        echo -e "${REDCOLOR}PHP Syntax check failed:${NOCOLOR}"
        echo -e "${REDCOLOR}PHP binary does not exist or is not in path: $PHP_BIN${NOCOLOR}"
        exit 1
fi

# get a list of staged files
for line in $(git diff-index --cached --full-index HEAD)
do
        # split needed values
        sha=$(echo $line | cut -d' ' -f4)
        temp=$(echo $line | cut -d' ' -f5)
        status=$(echo $temp | cut -d' ' -f1)
        filename=$(echo $temp | cut -d' ' -f2)

        # file extension
        ext=$(echo $filename | sed 's/^.*\.//')

        # only check files with php extension
        if [ $ext != "php" ]
        then
                continue
        fi

        # do not check deleted files
        if [[ $status = "D" ]]
        then
                continue
        fi

        # check the staged file content for syntax errors
        # using php -l (lint)
        result=$(git cat-file -p $sha | "$PHP_BIN" -n -l -ddisplay_errors\=1 -derror_reporting\=E_ALL -dlog_errrors\=0 2>&1)
        if [ $? -ne 0 ]
        then
                # Swap back in correct filenames
                errors=$(echo "$errors"; echo "$result" | grep ':' | sed -e "s@in - on@in $filename on@g")
        fi
done

if [ -n "$errors" ]
then
        echo -e "${REDCOLOR}********************ERRROR*********************${NOCOLOR}"
        echo -e "${REDCOLOR}**           PHP Syntax check failed         **${NOCOLOR}"
        echo -e "${REDCOLOR}***********************************************${NOCOLOR}"
        echo -e "$errors"
        echo ""
        exit 1
fi
unset IFS
echo -e "${GREENCOLOR}*********************OK**********************${NOCOLOR}"
echo -e "${GREENCOLOR}**          PHP Syntax check completed     **${NOCOLOR}"
echo -e "${GREENCOLOR}*********************************************${NOCOLOR}"


##################################
##  PHPCBF - AUTOFIX CODE STYLE ##
##################################
# get a list of staged files
for line in $(git diff-index --cached --full-index HEAD)
do
        status=$(echo $temp | cut -d' ' -f1)
        filename=$(echo $temp | cut -d' ' -f2)
        filename=$(echo $line | cut -d$'\t' -f2)
        ext=$(echo $filename | sed 's/^.*\.//')
        if [ $ext != "php" ]
        then
                continue
        fi

        # do not check deleted files
        if [[ $status = "D" ]]
        then
                continue
        fi
        result=$($PHPCBF_BIN  --standard=$STANDARD --report=simple $filename)
        if [[ $result='' ]]
        then
            errors=$result
        fi
done
##################################
##  PHPCS - CODE STYLE CHECKER  ##
##################################
# get a list of staged files
for line in $(git diff-index --cached --full-index HEAD)
do
        status=$(echo $temp | cut -d' ' -f1)
        filename=$(echo $temp | cut -d' ' -f2)
        filename=$(echo $line | cut -d$'\t' -f2)
        ext=$(echo $filename | sed 's/^.*\.//')

        # only check files with php extension
        if [ $ext != "php" ]
        then
                continue
        fi

        # do not check deleted files
        if [[ $status = "D" ]]
        then
                continue
        fi
        result=$($PHPCS_BIN  --standard=$STANDARD --report=simple $filename)
        if [[ $result='' ]]
        then
            errors=$result
        fi
done

if [ -n "$errors" ]
then
        echo -e "${REDCOLOR}********************ERRROR*********************${NOCOLOR}"
        echo -e "${REDCOLOR}**           Code style check failed         **${NOCOLOR}"
        echo -e "${REDCOLOR}***********************************************${NOCOLOR}"
        echo -e "$errors"
        echo ""
        exit 1
fi

echo -e "${GREENCOLOR}**********************OK***********************${NOCOLOR}"
echo -e "${GREENCOLOR}**           Code style check passed         **${NOCOLOR}"
echo -e "${GREENCOLOR}***********************************************${NOCOLOR}"
echo ""

if [ -z "$PHP_BIN" ]
then
        PHP_BIN=php
fi

errors=""
if ! which "$PHP_BIN" >/dev/null 2>&1
then
        echo "PHP Syntax check failed:"
        echo "PHP binary does not exist or is not in path: $PHP_BIN"
        exit 1
fi



